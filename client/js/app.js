angular.module('app', ['lbServices', 'ui.router']).config(
	['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$stateProvider.state(
			'place', {
				url: '',
				templateUrl: 'views/places.html',
				controller: 'PlaceController'
			});
		$urlRouterProvider.otherwise('place');
}]);
