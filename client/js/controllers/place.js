angular
	.module('app')
	.controller('PlaceController', ['$scope', '$state', 'Place', function($scope, $state, Place) {
		$scope.places = [];
		
		function getPlaces() {
			Place.find().$promise.then(function(results) {
				$scope.places = results;
			});
		}
		
		getPlaces();
		$scope.addPlace = function() {
			Place.create($scope.newPlace).$promise.then(function(place) {
				$scope.newPlace = '';
				$scope.placeForm.title.$setPristine();
				$scope.placeForm.address.$setPristine();
				$('.focus').focus();
				getPlaces();
			});
		};
		
		$scope.removePlace = function(item) {
			Place.deleteById(item).$promise.then(function() {
				getPlaces();
			});
		};
}]);
